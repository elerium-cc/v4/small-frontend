const { join } = require("path");

if (process.env.NODE_ENV === "development") require("dotenv").config({
    path: join(__dirname, "../.env")
});

const express = require("express");

const app = express();
app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

app.get("/register", (req, res) => {
    res.render("register", { API_URL: process.env.API_URL });
});

app.listen(process.env.PORT);